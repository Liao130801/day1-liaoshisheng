#O - Objective:
Today I learnt how to go about reading a book. I shared learning methods with my colleagues and learnt how to draw concept maps. We had an icebreaker activity of "Two Truths and One Lie". We also had a discussion on how to read a book and how to draw concept maps. Additionally, I learned how to communicate with the team.

**R - Reflect**:
I gained a lot from today's activities.

**I - Interpret**:
I believe today's learning was essential. It taught me the importance of effective communication within the team.

**D - Decision**:
The learning of how to communicate with the team will aid me greatly in future teamwork. It will enable me to fit in better in my next team.


